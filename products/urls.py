from django.conf.urls import patterns, include, url
from products import views

urlpatterns = patterns('',
    # url(r'^/api$', views.products_list_api),
    url(r'^$', views.products_list, name='list'),
    url(r'^categories/(?P<category_id>\d+)$', views.category_list, name='show-category'),
    url(r'^specialoffers$', views.specialoffers_list, name='special'),
    url(r'^add$', views.add_offer, name="add"),
    url(r'^register/$', views.register, name='register'),
    url(r'^login/$', views.user_login, name='login'),
    url(r'^logout/$', views.user_logout, name='logout'),
    url(r'^shopping/(?P<name>[\w\-]+)/$', views.shop, name='shopping'), 
    url(r'^prodcuts/(?P<product_id>\d+)/$', views.view_product, name='product'), 
    url(r'^shop/(?P<name>[\w\-]+)/$', views.view_shop, name='shop'),
    url(r'^category/(?P<category_id>[\w\-]+)/$', views.category, name='category'),
    url(r'^update/(?P<product_id>[\w\-]+)/$', views.editoffer, name='update'),
    url(r'^register/$', views.register, name='register'),
    url(r'^cart/add$', views.add_to_cart, name='add-to-cart'),
    url(r'^cart/show$', views.show_cart, name='show-cart'),
    url(r'^cart/remove$', views.remove_from_cart, name='remove-from-cart'),
    url(r'^cart/clear$', views.clear_cart, name='clear-cart'),
    url(r'^cart/update$', views.update_cart, name='cart-update'),
    url(r'^about$', views.about, name='about'),
    url(r'^contact$', views.contact, name='contact'),
    url(r'^checkout$', views.checkout, name='checkout'),
    url(r'^transactions/list$', views.list_transactions, name='transactions-list'),
    url(r'^editprofile$', views.edit_profile, name='editprofile'),
    
    )  

