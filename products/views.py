# -*- coding: utf-8 -*-            
from django.shortcuts import render
# from rest_framework.decorators import api_view, permission_classes
# from rest_framework import status, permissions
# from rest_framework.response import Response
from products.models import *
from products.forms import *
#from rango.forms import UserForm, UserProfileForm
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import HttpResponse

import random 
from carton.cart import Cart
from django.shortcuts import redirect
import json




# @api_view(['GET'])
# @permission_classes((permissions.AllowAny,))
# def products_list_api(request):
#     if request.method == 'GET':
#         #get only open offers and order them
#         # and available job through date
#         #filter with active offers
#         products = Product.objects.filter(is_active=True)
#         serializer = ProductSerializer(products, many=True)
#         return Response(serializer.data)



# @api_view(['GET'])
# @permission_classes((permissions.AllowAny,))
#def shop_products_api(request, shop):
   # if request.method == 'GET':
    #    products = Products.objects.filter(shop=shop)
     #   serializer = OfferSerializer(products, many=True)
      #  return Response(serializer.data)

def products_list(request):
    # import ipdb; ipdb.set_trace()
    if request.method == 'GET':
        # offers=Offer.objects.filter(end_date__gte=datetime.date.today())

        products=Product.objects.filter(is_active=True).order_by('-created_at')
        if len(products) >= 10:
            latest = products[0:10]
        else:
            latest = products

        featured = products.filter(special=True)
        if len(featured) >= 5:
            featured = featured[0:5]


        paginator = Paginator(products,10) # Show 25 offer per page

        page = request.GET.get('page')
        try:
            products = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            products = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            products = paginator.page(paginator.num_pages)
        context_dict = {'products': products,
                        'featured': featured,
                        'latest': latest,
                        'count':range(1, paginator.num_pages+1) }
        return render (request, "products/products.html", context_dict)

def about(request):
        return render (request, "products/about.html")
        

def category_list(request, category_id=None):
    # import ipdb; ipdb.set_trace()
    if request.method == 'GET':
        # offers=Offer.objects.filter(end_date__gte=datetime.date.today())
        # cat = Category.objects.get(id=category_id)
        # products=Product.objects.filter(category=cat)

        products=Product.objects.filter(category__id=category_id)
        paginator = Paginator(products,10) # Show 25 offer per page

        page = request.GET.get('page')
        try:
            products = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            products = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            products = paginator.page(paginator.num_pages)
        context_dict = {'products': products, 'count':range(1, paginator.num_pages+1) }
        return render (request, "products/products.html", context_dict)


def specialoffers_list(request):
    # import pdb; pdb.set_trace()
    if request.method == 'GET':
        offers=Offer.objects.filter(end_date__gte=datetime.date.today(),special=True)
    paginator = Paginator(offers,2) # Show 25 offer per page

    page = request.GET.get('page')
    try:
        offers_page = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        offers_page = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        offers_page = paginator.page(paginator.num_pages)
        #date = datetime.datetime.now()
        #offers=Offer.objects.all()
    context_dict = {'offers': offers_page }
    return render (request, "offers/offers.html", context_dict)

@login_required
def add_offer(request):
    #shop = request.user.shop
    import ipdb;ipdb.set_trace();

    if request.method == 'POST':
        form = ProductForm(request.POST, request.FILES)
        if form.is_valid():
           
            product= form.save(commit=False)
          
            # Did the user provide a profile picture?
            # If so, we need to get it from the input form and put it in the UserProfile model.
            if 'image' in request.FILES:
                product.image = request.FILES['image']

            # Now we save the UserProfile model instance.
                product.save()
            return HttpResponseRedirect(reverse('list'))
            

        else:
            print form.errors
    else:
        form = ProductForm()
    return render(request, "products/add.html", {'form': form})



def register(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('list'))
    registered = False
    if request.method == 'POST':
        user_form = UserForm(data=request.POST)
        shop_form = ShopForm(data=request.POST)
        if user_form.is_valid() and shop_form.is_valid():
            user = user_form.save()
            user.set_password(user.password)
            user.save()
            shop = shop_form.save(commit=False)
            shop.user = user
            shop.save()
            registered = True
            return HttpResponseRedirect(reverse('list'))
        else:
            user_form.errors, shop_form.errors
    else:
        user_form = UserForm()
        shop_form = ShopForm()
    return render(request,
            'offers/register.html',
            {'user_form': user_form, 'shop_form': shop_form, 'registered': registered} )


def user_login(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('list'))
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect(reverse('list'))
            else:
                return render (request, 'products/error.html', {'error_code': 'غير نشط','error_message': 'هذا الحساب غير مفعل'})
        else:
            return render (request, 'products/login.html', {'error': 'بيانات التسجيل غير صحيحة'})

    else:
        return render (request, 'products/login.html', {})

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('list'))


def shop(request, name):

    # Create a context dictionary which we can pass to the template rendering engine.
    context_dict = {}

    try:
       # import pdb; pdb.set_trace()
        shop = Shop.objects.get(name=name)
        context_dict['name']=shop.name
        offers = Offer.objects.filter(shop=shop)
        context_dict['offers'] = offers
        context_dict['shop'] = shop
    except Shop.DoesNotExist:
        print "Shop does not exist"
    return render(request, 'offers/shop.html', context_dict)






@login_required
def view_product(request, product_id):
    # Create a context dictionary which we can pass to the template rendering engine.
    context_dict = {}

    try:
        product= Product.objects.get(id=product_id)
        context_dict['product'] = product
        # We also add the category object from the database to the context dictionary.
        # We'll use this in the template to verify that the category exists.
       # context_dict['shop'] = shop
    except Product.DoesNotExist:
        # We get here if we didn't find the specified category.
        # Don't do anything - the template displays the "no category" message for us.
        pass

    # Go render the response and return it to the client.
    return render(request, 'products/view_product.html', context_dict)





def view_shop(request, name):

    # Create a context dictionary which we can pass to the template rendering engine.
    context_dict = {}

    try:
        # import pdb; pdb.set_trace()
        shop= Shop.objects.get(name=name)
        context_dict['shop'] = shop
    except Shop.DoesNotExist:
        
        print "ERROR"

    # Go render the response and return it to the client.
    return render(request, 'offers/view_shop.html', context_dict)




def category(request, category_id):

    # Create a context dictionary which we can pass to the template rendering engine.
    context_dict = {}

    try:
       # import pdb; pdb.set_trace()
        category = get_object_or_404(Category, category_id=category_id)
        offers = Offer.objects.filter(category=category)
        context_dict['category_id']=category_id
        context_dict['offers']=offers
    except Category.DoesNotExist:
        print "Shop does not exist"
    return render(request, 'products/category.html', context_dict)



@login_required
def editoffer(request,product_id):
    if request.POST:
        offerform = OfferForm(request.POST)

        if offerform.is_valid():

            offer = Offer.objects.get(id=product_id)
            offerform = OfferForm(request.POST, instance = offer)
            offerform.save()
            return HttpResponseRedirect(reverse('list'))
    else:
        offer = Offer.objects.get(id = product_id)       
        offerform = OfferForm(instance=offer)

        return render(request, 'offers/editoffer.html',{ 'form':offerform })



def contact(request):
    if request.POST:
        message_form = MessageForm(request.POST)

        if message_form.is_valid():
            message_form.save()
            return HttpResponseRedirect(reverse('list'))
    else:       
        message_form = MessageForm()
        return render(request, 'products/contact.html',{ 'form':message_form })






def register(request):
   # import ipdb;ipdb.set_trace()
    # A boolean value for telling the template whether the registration was successful.
    # Set to False initially. Code changes value to True when registration succeeds.
    registered = False

    # If it's a HTTP POST, we're interested in processing form data.
    if request.method == 'POST':
        # Attempt to grab information from the raw form information.
        # Note that we make use of both UserForm and UserProfileForm.
        user_form = UserForm(data=request.POST)
        profile_form = UserProfileForm(data=request.POST)

        # If the two forms are valid...
        if user_form.is_valid() and profile_form.is_valid():
            # Save the user's form data to the database.
            user = user_form.save()

            # Now we hash the password with the set_password method.
            # Once hashed, we can update the user object.
            user.set_password(user.password)
            user.save()

            # Now sort out the UserProfile instance.
            # Since we need to set the user attribute ourselves, we set commit=False.
            # This delays saving the model until we're ready to avoid integrity problems.
            profile = profile_form.save(commit=False)
            profile.user = user

            # Did the user provide a profile picture?
            # If so, we need to get it from the input form and put it in the UserProfile model.
           # if 'picture' in request.FILES:
            #    profile.picture = request.FILES['picture']

            # Now we save the UserProfile model instance.
            profile.save()

            # Update our variable to tell the template registration was successful.
            registered = True
            return redirect ('list')

        # Invalid form or forms - mistakes or something else?
        # Print problems to the terminal.
        # They'll also be shown to the user.
        else:
            print user_form.errors, profile_form.errors

    # Not a HTTP POST, so we render our form using two ModelForm instances.
    # These forms will be blank, ready for user input.
    else:
        user_form = UserForm()
        profile_form = UserProfileForm()

    # Render the template depending on the context.
    return render(request,
            'products/register_page.html',
            {'user_form': user_form, 'profile_form': profile_form, 'registered': registered} )


def add_to_cart(request):
    if request.is_ajax() and request.method == "POST":
        product_id = request.POST.get('product_id')
        quantity = request.POST.get('qty')
        cart = Cart(request.session)
        product = Product.objects.get(id=product_id)
        if product in cart.products:
            return HttpResponse('0')
        cart.add(product, price=product.price, quantity=quantity)
        return HttpResponse("1")
    else:
        return render(request,
            'products/error.html',
            {'error_code': '400', 'error_message': 'حدث خطأ أثناء تنفيذ طلبك'} ) 

@login_required
def show_cart(request):
    return render(request, 'products/cart.html')

def remove_from_cart(request):
    if request.is_ajax() and request.method == "POST":
        cart = Cart(request.session)
        product = Product.objects.get(id=request.POST.get('product_id'))
        cart.remove(product)
        return HttpResponse("Removed")
    else:
        return render(request,
            'products/error.html',
            {'error_code': '400', 'error_message': 'حدث خطأ أثناء تنفيذ طلبك'} ) 


def clear_cart(request):
    cart = Cart(request.session)
    cart.clear()
    return HttpResponseRedirect(reverse('list'))

def update_cart(request):
    cart = Cart(request.session)
    products_list = request.POST.get('products_list')
    products_list = json.loads(products_list)
    for prod_dict in products_list:
        product = Product.objects.get(id=prod_dict['product_id'])
        cart.set_quantity(product, prod_dict['qty'])
        if prod_dict['remove']:
            cart.remove(product)
    return HttpResponse("updated")

def checkout(request):
    if request.method == "POST":
        receipt_no = request.POST.get('receipt_no')
        cart = Cart(request.session)
        transaction = Transaction()
        transaction.total = cart.total
        transaction.status = 'new'
        transaction.receipt_no = receipt_no

        transaction.user_profile = request.user.userprofile
        transaction.save()
        for item in cart.items:
            transaction_item = TransactionItem()
            transaction_item.product = item.product
            transaction_item.quantity = item.quantity
            transaction_item.subtotal = item.subtotal
            transaction_item.transaction = transaction
            transaction_item.save()
        cart.clear()
        return HttpResponseRedirect(reverse('list'))
    else: 
        return render(request,
            'products/checkout.html',
            {} )  

def list_transactions(request):
    user_profile = request.user.userprofile
    transactions = Transaction.objects.filter(user_profile=user_profile)
    return render(request,
            'products/transactions.html',
            {'transactions': transactions} )    



def edit_profile(request):
    if request.method == "GET":
        profile_form = UserProfileForm(instance=request.user.userprofile)
        return render(request,
                'products/edit.html',
                {'profile_form': profile_form} )    
    else:
        profile_form = UserProfileForm(data=request.POST, instance=request.user.userprofile)

        if profile_form.is_valid():
            profile_form.save()
            return HttpResponseRedirect(reverse('list'))
        else:
            return render(request,
                    'products/edit.html',
                    {'profile_form': profile_form} )
