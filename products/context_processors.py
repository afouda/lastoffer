from products.models import *
from carton.cart import Cart

def categories_processor(request):
	categories = Category.objects.all()            
	return {'categories': categories}


def cart(request):
    cart = Cart(request.session)
    total = 0
    for product in cart.products:
    	total += product.price
    	
    return {'cart': cart, 'total': total}