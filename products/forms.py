# -*- coding: utf-8 -*-
from django import forms
from products.models import *
import html5.forms.widgets as html5_widgets
from django.contrib.auth.models import User


class ProductForm(forms.ModelForm):
    # product_name = forms.CharField(label="اسم المنتج", max_length=255)
    # url = forms.CharField(label="رابط المنتج")
    # end_date = forms.DateField(label="تاريخ انتهاء العرض", required=False, widget=html5_widgets.DateInput)
    # image = forms.ImageField(label="صورة العرض", required=True)
    # price = forms.FloatField(label="السعر قبل الخصم")
    # after_price = forms.FloatField(label="السعر بعد الخصم")
    # special = forms.BooleanField(label="عرض مميز", required=False)
    # #desc = forms.CharField(widget=forms.Textarea)
    # desc = forms.CharField( label="الوصف")

    
    def clean(self):
        price = self.cleaned_data.get("price")
        before_price = self.cleaned_data.get("before_price")
        if price > before_price :
            msg = u"السعر قبل الخصم يجب أن يكون أكبر من أو يساوى السعر بعد الخصم"
            self._errors["before_price"] = self.error_class([msg])


    class Meta:
        model = Product
        fields = '__all__'
        # exclude = ('is_active','created_at', 'shop')
    


class UserForm(forms.ModelForm):
    username = forms.CharField(label="اسم المستخدم")
    email = forms.EmailField(label="البريد الإلكترونى")
    password = forms.CharField(label="كلمة المرور",widget=forms.PasswordInput())
    class Meta:
        model = User
        fields = ('username', 'email', 'password')

    
class MessageForm(forms.ModelForm):
    name = forms.CharField(label="الاسم")
    email = forms.EmailField(label="البريد الإلكترونى")
    message_body = forms.CharField(label="الرسالة", widget=forms.Textarea)
    class Meta:
        model = Message
        fields = ('name', 'email', 'message_body')

class UserProfileForm(forms.ModelForm):
    address = forms.CharField(label="العنوان")
    mobile = forms.CharField(label="رقم الموبايل") 
    class Meta:
        model = UserProfile
        fields = ('address', 'mobile') 