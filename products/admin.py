# -*- coding: utf-8 -*-
from django.contrib import admin
from products.models import *
from products.forms import *
from django.contrib import messages


# Register your models here.
#admin.site.register(Shop)
# admin.site.register(Product)
admin.site.register(Category)


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    form = ProductForm
    list_display = ('name', 'price', 'is_active', 'category', 'created_at')

@admin.register(UserProfile)
class UserProfile(admin.ModelAdmin):
    list_display = ('user', 'address', 'mobile')

@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'message_body')
    readonly_fields = ('name', 'email', 'message_body')

class UserInline(admin.TabularInline):
    model = User
    # list_display = ('username', 'password')

class TransactionItemInline(admin.TabularInline):
    model = TransactionItem
    readonly_fields = ('product', 'quantity', 'subtotal')
    list_display = ('product', 'quantity', 'subtotal')
    can_delete = False
    max_num = 0

@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    model = Transaction
    readonly_fields = ('user_profile', 'total', 'receipt_no', 'created_at') 
    list_display = ('user_profile', 'status', 'total', 'receipt_no', 'created_at', 'get_address',)
    inlines = [TransactionItemInline,]

    def get_address(self, obj):
        return obj.user_profile.address

    get_address.short_description = 'العنوان'

