# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0003_auto_20161230_2333'),
    ]

    operations = [
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'\xd8\xa7\xd9\x84\xd8\xa7\xd8\xb3\xd9\x85')),
                ('email', models.EmailField(max_length=70, verbose_name=b'\xd8\xa7\xd9\x84\xd8\xa8\xd8\xb1\xd9\x8a\xd8\xaf \xd8\xa7\xd9\x84\xd8\xa5\xd9\x84\xd9\x83\xd8\xaa\xd8\xb1\xd9\x88\xd9\x86\xd9\x89')),
                ('message_body', models.CharField(max_length=255, verbose_name=b'\xd8\xa7\xd9\x84\xd8\xb1\xd8\xb3\xd8\xa7\xd9\x84\xd8\xa9')),
            ],
            options={
                'verbose_name': '\u0627\u0644\u0631\u0633\u0627\u0644\u0629',
                'verbose_name_plural': '\u0627\u0644\u0631\u0633\u0627\u0626\u0644',
            },
            bases=(models.Model,),
        ),
        migrations.DeleteModel(
            name='Contact',
        ),
    ]
