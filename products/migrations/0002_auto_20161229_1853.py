# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='end_date',
        ),
        migrations.RemoveField(
            model_name='product',
            name='url',
        ),
        migrations.AlterField(
            model_name='category',
            name='name',
            field=models.CharField(unique=True, max_length=255, verbose_name=b'\xd8\xa7\xd8\xb3\xd9\x85 \xd8\xa7\xd9\x84\xd8\xaa\xd8\xb5\xd9\x86\xd9\x8a\xd9\x81'),
        ),
        migrations.AlterField(
            model_name='product',
            name='before_price',
            field=models.FloatField(null=True, verbose_name=b'\xd8\xa7\xd9\x84\xd8\xb3\xd8\xb9\xd8\xb1 \xd9\x82\xd8\xa8\xd9\x84 \xd8\xa7\xd9\x84\xd8\xae\xd8\xb5\xd9\x85'),
        ),
        migrations.AlterField(
            model_name='product',
            name='desc',
            field=models.CharField(max_length=255, null=True, verbose_name=b'\xd8\xa7\xd9\x84\xd9\x88\xd8\xb5\xd9\x81'),
        ),
        migrations.AlterField(
            model_name='product',
            name='image',
            field=models.ImageField(upload_to=b'static/images/product', null=True, verbose_name=b'\xd8\xb5\xd9\x88\xd8\xb1\xd8\xa9 \xd8\xa7\xd9\x84\xd9\x85\xd9\x86\xd8\xaa\xd8\xac'),
        ),
        migrations.AlterField(
            model_name='product',
            name='is_active',
            field=models.BooleanField(default=False, verbose_name=b'\xd9\x85\xd9\x81\xd8\xb9\xd9\x84'),
        ),
        migrations.AlterField(
            model_name='product',
            name='name',
            field=models.CharField(max_length=255, verbose_name=b'\xd8\xa7\xd8\xb3\xd9\x85 \xd8\xa7\xd9\x84\xd9\x85\xd9\x86\xd8\xaa\xd8\xac'),
        ),
        migrations.AlterField(
            model_name='product',
            name='price',
            field=models.FloatField(null=True, verbose_name=b'\xd8\xa7\xd9\x84\xd8\xb3\xd8\xb9\xd8\xb1 \xd8\xa7\xd9\x84\xd8\xad\xd8\xa7\xd9\x84\xd9\x89'),
        ),
        migrations.AlterField(
            model_name='product',
            name='special',
            field=models.BooleanField(default=False, verbose_name=b'\xd9\x85\xd9\x86\xd8\xaa\xd8\xac \xd8\xae\xd8\xa7\xd8\xb5'),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='address',
            field=models.CharField(max_length=255, verbose_name=b'\xd8\xa7\xef\xbb\xbb\xd8\xb3\xd9\x85'),
        ),
        migrations.AlterField(
            model_name='userprofile',
            name='mobile',
            field=models.CharField(max_length=255, null=True, verbose_name=b'\xd8\xb1\xd9\x82\xd9\x85 \xd8\xa7\xd9\x84\xd9\x85\xd9\x88\xd8\xa8\xd8\xa7\xd9\x8a\xd9\x84'),
        ),
    ]
