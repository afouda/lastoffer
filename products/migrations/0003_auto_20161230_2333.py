# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0002_auto_20161229_1853'),
    ]

    operations = [
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'\xd8\xa7\xd8\xb3\xd9\x85 \xd8\xa7\xd9\x84\xd9\x85\xd9\x86\xd8\xaa\xd8\xac')),
                ('email', models.EmailField(max_length=70)),
                ('message_body', models.TextField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterModelOptions(
            name='category',
            options={'verbose_name': '\u0627\u0644\u062a\u0635\u0646\u064a\u0641', 'verbose_name_plural': '\u0627\u0644\u062a\u0635\u0646\u064a\u0641\u0627\u062a'},
        ),
        migrations.AlterModelOptions(
            name='product',
            options={'verbose_name': '\u0627\u0644\u0645\u0646\u062a\u062c', 'verbose_name_plural': '\u0627\u0644\u0645\u0646\u062a\u062c\u0627\u062a'},
        ),
        migrations.AlterModelOptions(
            name='transaction',
            options={'verbose_name': '\u0639\u0645\u0644\u064a\u0629 \u0627\u0644\u0628\u064a\u0639', 'verbose_name_plural': '\u0639\u0645\u0644\u064a\u0627\u062a \u0627\u0644\u0628\u064a\u0639'},
        ),
        migrations.AlterModelOptions(
            name='userprofile',
            options={'verbose_name': '\u0645\u0644\u0641 \u0627\u0644\u0645\u0633\u062a\u062e\u062f\u0645', 'verbose_name_plural': '\u0645\u0644\u0641\u0627\u062a \u0627\u0644\u0645\u0633\u062a\u062e\u062f\u0645\u064a\u0646'},
        ),
        migrations.AlterField(
            model_name='transaction',
            name='status',
            field=models.CharField(max_length=255, choices=[('new', '\u062c\u062f\u064a\u062f'), ('accepted', '\u062a\u0645 \u0627\u0644\u0642\u0628\u0648\u0644'), ('processing', '\u0642\u064a\u062f \u0627\u0644\u062a\u0646\u0641\u064a\u0630'), ('done', '\u062a\u0645')]),
        ),
    ]
