# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
import datetime

class UserProfile(models.Model):
    # This line is required. Links UserProfile to a User model instance.
    user = models.OneToOneField(User)

    # The additional attributes we wish to include.
    address = models.CharField("اﻻسم",max_length=255)
    mobile = models.CharField("رقم الموبايل",max_length=255,null=True)

    # Override the __unicode__() method to return out something meaningful!
    def __unicode__(self):
        return self.user.username

    class Meta:
      verbose_name = "ملف المستخدم"
      verbose_name_plural = "ملفات المستخدمين"

class Category(models.Model):
    name=models.CharField("اسم التصنيف",max_length=255, unique=True)


    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = "التصنيف"
        verbose_name_plural = "التصنيفات"

class Product(models.Model):
   #shop = models.ForeignKey(Shop, on_delete=models.CASCADE)
    name = models.CharField("اسم المنتج", max_length=255)
    # url = models.URLField("", null=True)
    before_price = models.FloatField("السعر قبل الخصم", null=True)
    price = models.FloatField("السعر الحالى", null=True)
    # end_date = models.DateField(null=True)
    is_active = models.BooleanField("مفعل", default=False)
    image = models.ImageField("صورة المنتج",null=True, upload_to='static/images/product')
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=True)
    special = models.BooleanField("منتج خاص",default=False)
    desc = models.CharField("الوصف",max_length=255, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = "المنتج"
        verbose_name_plural = "المنتجات"


    # def save(self, *args, **kwargs):
    #     if self.before_price < self.price:
    #         raise ValueError("Updating the value of creator isn't allowed")
    #     super(Product, self).save(*args, **kwargs)

class TransactionItem(models.Model):
    product = models.ForeignKey(Product)
    quantity = models.IntegerField(null=True)
    subtotal = models.FloatField(null=True)
    transaction = models.ForeignKey('Transaction')

    
STATES = (
    (u'new',u'جديد'),
    (u'accepted',u'تم القبول'),
    (u'processing',u'قيد التنفيذ'),
    (u'done',u'تم'),
    )   
class Transaction(models.Model):
    user_profile = models.ForeignKey(UserProfile)
    status = models.CharField(choices=STATES, max_length=255)
    total = models.FloatField(null=True)
    receipt_no = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = "عملية البيع"
        verbose_name_plural = "عمليات البيع"



class Message(models.Model):
    name = models.CharField("الاسم", max_length=255)
    email = models.EmailField("البريد الإلكترونى", max_length=70,)
    message_body =models.CharField("الرسالة", max_length=255)
   

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name = "الرسالة"
        verbose_name_plural = "الرسائل"